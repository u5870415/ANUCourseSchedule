/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QMenu>
#include <QMenuBar>
#include <QPainter>
#include <QDateTime>
#include <QJsonArray>
#include <QJsonDocument>
#include <QApplication>
#include <QFile>
#include <QFileDialog>

#include "mainwindow.h"

#include <QDebug>

#define StartHour 8
#define EndHour 19

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setAutoFillBackground(true);

    QMenu *file=new QMenu("&File", this);
    menuBar()->addMenu(file);
    QAction *open=new QAction("&Open", this);
    connect(open, &QAction::triggered, [=]
    {
        QString filePath=QFileDialog::getOpenFileName(this);
        if(filePath.isEmpty())
        {
            return;
        }
        loadFromFile(filePath);
    });
    file->addAction(open);
    QAction *exit=new QAction("E&xit", this);
    connect(exit, &QAction::triggered, this, &MainWindow::close);
    file->addAction(exit);

    QMenu *week=new QMenu("&Week", this);
    menuBar()->addMenu(week);
    QAction *next=new QAction("&Next Week", this);
    next->setShortcut(QKeySequence(Qt::Key_N));
    connect(next, &QAction::triggered, [=]
    {
        if(m_weekNumber==52)
        {
            return;
        }
        setWeek(m_weekNumber+1);
    });
    week->addAction(next);
    QAction *previous=new QAction("&Previous Week", this);
    previous->setShortcut(QKeySequence(Qt::Key_P));
    connect(previous, &QAction::triggered, [=]
    {
        if(m_weekNumber==1)
        {
            return;
        }
        setWeek(m_weekNumber-1);
    });
    week->addAction(previous);
    QAction *refresh=new QAction("&Current Week", this);
    refresh->setShortcut(QKeySequence(Qt::Key_F5));
    connect(refresh, &QAction::triggered, [=]
    {
        setWeek(QDateTime::currentDateTime().date().weekNumber());
    });
    week->addAction(refresh);
    if(QApplication::arguments().size()>1)
    {
        loadFromFile(QApplication::arguments().at(1));
    }
}

void MainWindow::setWeek(int week)
{
    m_weekNumber=week;
    m_items.clear();
    for(auto item : m_totalItems)
    {
        if(item.weeks.contains(week))
        {
            m_items.append(item);
        }
    }
    setWindowTitle(QString("Timetable - Week %1").arg(QString::number(m_weekNumber)));
    update();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QMainWindow::paintEvent(event);
    QPainter painter(this);
    renderMap(&painter, QRect(0, menuBar()->height(), width(), height()-menuBar()->height()));
}

void MainWindow::loadFromFile(const QString &filePath)
{
    QFile timeTableFile(filePath);
    if(timeTableFile.open(QIODevice::ReadOnly))
    {
        QByteArray fileContent=timeTableFile.readAll();
        timeTableFile.close();
        loadFromList(fileContent);
    }
}

void MainWindow::loadFromList(const QByteArray &listData)
{
    QJsonArray list=QJsonDocument::fromJson(listData).array();
    bool okay;
    m_totalItems=QList<CourseItem>();
    for(auto i : list)
    {
        CourseItem item=getItem(i.toObject(), &okay);
        if(okay)
        {
            m_totalItems.append(item);
        }
    }
    setWeek(QDateTime::currentDateTime().date().weekNumber());
}

CourseItem MainWindow::getItem(const QJsonObject &object, bool *ok)
{
    CourseItem item;
    (*ok)=true;
    item.location=object.value("location").toString();
    item.title=object.value("title").toString();
    item.type=object.value("type").toString();
    item.titleColor=QColor(0x0f, 0x36, 0x5a);
    item.startTime=getTime(object.value("start").toString());
    item.endTime=getTime(object.value("end").toString());
    if(item.endTime<item.startTime)
    {
        (*ok)=false;
    }
    item.day=object.value("day").toInt();
    item.weeks=getWeekList(object.value("weeks").toString());
    return item;
}

inline QTime MainWindow::getTime(const QString &text)
{
    int pos=text.indexOf(':');
    if(pos==-1)
    {
        return QTime(text.toInt(), 0);
    }
    else
    {
        return QTime(text.left(pos).toInt(), text.mid(pos+1).toInt());
    }
}

QList<int> MainWindow::getWeekList(const QString &weeks)
{
    QStringList weekParts=weeks.split(',', QString::SkipEmptyParts);
    QList<int> week;
    for(auto i:weekParts)
    {
        QString trimText=i.trimmed();
        int slash=trimText.indexOf('-');
        if(slash==-1)
        {
            week.append(trimText.toInt());
        }
        else
        {
            int start=trimText.left(slash).toInt(), end=trimText.mid(slash+1).toInt();
            for(int j=start; j<=end; ++j)
            {
                week.append(j);
            }
        }
    }
    return week;
}

void MainWindow::renderMap(QPainter *painter, QRect area)
{
    painter->setPen(QColor(0x99, 0x99, 0x99));
    painter->setBrush(QColor(0x6d, 0x6d, 0x6d));
    painter->drawRect(area.x(), area.y(), 46, area.height());
    int split=EndHour-StartHour, itemHeight=qMax(20, area.height()/split/2),
            posY=area.y();
    painter->setPen(QColor(0xdd, 0xdd, 0xdd));
    QFont textFont=font();
    textFont.setFamily("Arial");
    textFont.setBold(true);
    textFont.setPixelSize(20);
    painter->setFont(textFont);
    int columnWidth=(area.width()-46)/5, posX=46;
    for(int i=0; i<5; ++i)
    {
        if(QDateTime::currentDateTime().date().dayOfWeek()==i+1 &&
                QDateTime::currentDateTime().date().weekNumber()==m_weekNumber)
        {
            painter->fillRect(posX, area.top(), columnWidth, area.height(),
                              QColor(181, 187, 209));
        }
        painter->drawLine(posX, area.top(), posX, area.bottom());
        posX+=columnWidth;
    }

    for(int i=0; i<split; ++i)
    {
        int hour=StartHour+i;
        if(hour>12)
        {
            hour-=12;
        }
        painter->drawLine(area.left(), posY, area.right(), posY);
        posY+=itemHeight;
        painter->drawLine(area.left()+46, posY, area.right(), posY);
        posY+=itemHeight;
        painter->drawText(area.left()+5, posY-(itemHeight<<1)+5, 41, itemHeight<<1,
                          Qt::AlignLeft, QString::number(hour));
    }

    int itemLeft, itemTop, itemBottom, courseHeight;

    textFont.setPixelSize(14);
    textFont.setBold(false);
    painter->setFont(textFont);
    QFontMetrics cMetrics(textFont);
    QColor contentColor;
    for(auto i:m_items)
    {
        itemLeft=columnWidth*(i.day-1)+46;
        itemTop=(i.startTime.hour()-StartHour)*(itemHeight<<1) +
                (((qreal)i.startTime.minute()/60.0)*(itemHeight*2.0))+area.top();
        itemBottom=(i.endTime.hour()-StartHour)*(itemHeight<<1) +
                (((qreal)i.endTime.minute()/60.0)*(itemHeight*2.0))+area.top();
        courseHeight=itemBottom-itemTop;
        contentColor.setHsv(i.titleColor.hue(), i.titleColor.saturation(), 102);
        painter->setPen(contentColor);
        painter->setBrush(contentColor);
        painter->drawRoundRect(QRect(itemLeft+1, itemTop, columnWidth-2, courseHeight-2), 5, 5);
        painter->setPen(QColor(0xFF, 0xFF, 0xFF));
        painter->drawText(itemLeft+5, itemTop, columnWidth, 20,
                          Qt::AlignLeft | Qt::AlignVCenter, cMetrics.elidedText(i.title, Qt::ElideRight, columnWidth));
        itemTop+=20;
        courseHeight-=20;
        contentColor.setHsv(i.titleColor.hue(), i.titleColor.saturation(), 140);
        painter->setPen(contentColor);
        painter->setBrush(contentColor);
        painter->drawRect(itemLeft+1, itemTop, columnWidth-2, courseHeight-2);
        painter->setPen(QColor(0xFF, 0xFF, 0xFF));
        painter->drawText(itemLeft+5, itemTop, columnWidth, courseHeight,
                          Qt::AlignLeft|Qt::AlignTop,
                          i.type + "\n" + i.location + "\n" + i.startTime.toString("h:mm") + " - " + i.endTime.toString("h:mm"));
    }
}
