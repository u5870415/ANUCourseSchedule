/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJsonObject>

#include "courseglobal.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

signals:

public slots:
    inline void setWeek(int week);

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    inline void loadFromFile(const QString &filePath);
    inline void loadFromList(const QByteArray &listData);
    inline CourseItem getItem(const QJsonObject &object, bool *ok);
    inline QTime getTime(const QString &text);
    inline QList<int> getWeekList(const QString &weeks);
    inline void renderMap(QPainter *painter, QRect area);
    QList<CourseItem> m_items, m_totalItems;
    int m_weekNumber;
};

#endif // MAINWINDOW_H
